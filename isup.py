import xchat
import socket

__module_name__ = "IsUp Site checker" 
__module_version__ = "3.0" 
__module_description__ = "Site status checker - Coded by Aha2Y" 

print "\0034",__module_name__, __module_version__,"has been loaded\003"

def is_up(word, word_eol, userdata):
   if "http://" in word_eol[1]:
       xchat.emit_print("Generic Message", "[IsUp]", "Error: Please remove the http:// part.")
       return xchat.EAT_ALL;
   else:
       host = word[1]
       port = 80
       checksock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
       try:
           checksock.connect((host, port))
           checksock.shutdown(2)
           xchat.emit_print("Generic Message", "[IsUp]", "%s is \0033online" % host)
           return xchat.EAT_ALL;
       except:
           xchat.emit_print("Generic Message", "[IsUp]", "%s is \0034offline" % host)
           return xchat.EAT_ALL;

xchat.hook_command("isup", is_up)
